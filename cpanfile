requires 'PHP::Serialization';
requires 'DBI';
requires 'DBD::mysql';
requires 'SOAP::Lite';
requires 'Test2::V0';
requires 'App::Cmd::Tester';
requires 'Config::General';
requires 'SQL::Abstract::More';
requires 'SQL::Abstract::Plugin::InsertMulti';
requires 'Readonly';
requires 'Class::Tiny';
requires 'String::Flogger';
requires 'Log::Any';
requires 'Log::Log4perl';
requires 'Log::Any::Adapter::Log4perl';
requires 'common::sense';
requires 'JSON::XS';
requires 'OpenAPI::Client';
requires 'URL::Encode';
requires 'Data::Dumper::Perltidy';
requires 'JSON';
requires 'Math::Calc::Parser';
requires 'XML::Simple';
requires 'Thread::Queue';
requires 'App::Cmd';
requires 'Path::Tiny';
requires 'Data::Printer';
requires 'LWP::ConsoleLogger::Easy';
requires 'Module::Load::Conditional';