package SI::Supplier::marvel;
use common::sense;
use Log::Any qw($log);
use LWP::UserAgent;
use HTTP::Request::Common;
use URI;
use Readonly;
use JSON::XS qw( encode_json decode_json );
use List::MoreUtils qw( natatime );
use URL::Encode qw( url_encode_utf8 url_decode_utf8 url_decode url_encode );

Readonly my $ITEMS_AVAIL_MAX_ITEM => 1000;

use parent 'SI::Supplier::base';
use Class::Tiny {
    url        => 'https://b2b.marvel.ru/Api/',
    username   => '',
    password   => '',
    secret_key => '',
    ua         => sub { LWP::UserAgent->new },
    json       => sub { JSON::XS->new->utf8( 0 ) },
    currency   => 'RUB',
};

sub items_avail {
    my ( $self, %args ) = @_;

    my $product_ids = $args{product_ids};
    my $product_id_iter = natatime( $ITEMS_AVAIL_MAX_ITEM, map { $_ } @$product_ids );
    my @items_avail;
    while ( my @product_ids = $product_id_iter->() ) {
        my $avail_part = $self->_items_avail( \@product_ids );
        push @items_avail, @$avail_part;
    }
    my %items_avail = map { $_->{item_id} => $_ } @items_avail;
    return [ values %items_avail ];
}

sub _items_avail {
    my ( $self, $product_ids ) = @_;
    my %options = (
        "WareItem" => [ map +{ ItemId => $_ }, @$product_ids ]
    );
    my $response = $self->request( 'GetItems', {
        packStatus => 1,
        items      => encode_json( \%options )
    } );

    my @items;
    ITEMS:
    for my $category_item ( @{ $response->{CategoryItem} } ) {
        my $ware_price = $category_item->{WarePriceRUB};
        $ware_price   =~ s/\,/\./mxs;
        $ware_price   += 0;
        $ware_price   += $self->delivery_fee_for( $ware_price );

        push @items, {
            item_id => url_decode_utf8( $category_item->{WareArticle} ),
            cnt     => $category_item->{AvailableForB2BOrderQty} +0,
            price   => $ware_price,
        };
    }

    return \@items;
}

sub request {
    my ( $self, $method, $data ) = @_;

    my $url = URI->new( $self->url );
    my @path = $url->path_segments;
    $url->path_segments( @path, $method );

    my %form = (
        user           => $self->username,
        password       => $self->password,
        responseFormat => 1,
        %$data
    );
    my $res     = $self->ua->post( $url, \%form );
    my $content = $self->json->decode( $res->content );

    return $content->{Body};
}

1;
