package SI::Supplier::base;
use common::sense;
use Log::Any qw( $log );
use Readonly;
use Time::Piece;
use Time::Seconds;
use Path::Tiny;
use HTTP::Tiny;
use XML::Simple;

Readonly my $CBR_RATES_URL  => 'https://cbr.ru/scripts/XML_daily.asp';
Readonly my $USD_RATE_CACHE => '/tmp/usd_rate';

use Class::Tiny {
    delivery_price               => 0,
    delivery_currency            => 'RUB',
    delivery_second_price_border => 0,
    delivery_second_price        => 0,
    usd_rate                     => sub { $_[0]->_build_usd_rate },
};

sub delivery_fee_for {
    my ( $self, $price ) = @_;

    return 0 if ( !$price );
    return 0 if ( !$self->delivery_price );

    $log->tracef( "Input price is: %s. Supplier currency is: %s. Delivery currency is: %s",
                  $price, $self->currency, $self->delivery_currency );
    if ( $self->currency eq 'USD' && $self->delivery_currency eq 'RUB' ) {
        $price *= $self->usd_rate;
    }
    elsif ( $self->currency eq 'RUB' && $self->delivery_currency eq 'USD' ) {
        $price /= $self->usd_rate;
    }
    $log->tracef( "Product price in delivery currency is: %s.", $price );

    my $fee = 0;
    if ( $price < $self->delivery_second_price_border ) {
        $fee = $self->delivery_price;
        $log->tracef( 'Setting fee = delivery price: %s', $fee );
    }
    else {
        $fee = $self->delivery_second_price;
        $log->tracef( 'Setting fee = delivery second price: %s', $fee );
    }

    if ( $self->currency eq 'USD' && $self->delivery_currency eq 'RUB' ) {
        $fee /= $self->usd_rate;
    }
    elsif ( $self->currency eq 'RUB' && $self->delivery_currency eq 'USD' ) {
        $fee *= $self->usd_rate;
    }
    $log->tracef( "Product delivery fee in supplier currency is: %s", $fee );

    return $fee;
}

sub load_cache_usd_rate {
    my $self = shift;
    my $rate;

    my $rate_file = path( $USD_RATE_CACHE );
    return undef if ( !$rate_file->exists );

    my $mtime     = Time::Piece->strptime(  $rate_file->stat->mtime, "%s" );
    my $mtimediff = ( localtime ) - $mtime;
    return undef if ( $mtimediff >= ONE_DAY );

    my $rate = $rate_file->slurp;
    $log->tracef("Got cached usd rate: %f", $rate);

    return $rate;
}
             

sub save_cache_usd_rate {
    my ( $self, $usd_rate ) = @_;

    my $rate_file = path( $USD_RATE_CACHE );
    $log->tracef("Saving usd rate to cache %f", $usd_rate);
    $rate_file->spew( $usd_rate );

    return;
}

# default way to load usd rate
sub _build_usd_rate {
    my $self = shift;

    my $cached_usd_rate = $self->load_cache_usd_rate;
    return $cached_usd_rate if ( $cached_usd_rate );

    my $response = HTTP::Tiny->new->get( $CBR_RATES_URL );
    die "Failed to load cbr rate" if ( !$response->{success} );

    my $data = XML::Simple::XMLin( $response->{content} );
    my ( $usd_data ) = grep { $_->{CharCode} eq 'USD' } @{ $data->{Valute} };
    $usd_data->{Value} =~ s/\,/\./msx;
    $self->save_cache_usd_rate( $usd_data->{Value} );

    return $usd_data->{Value};
}

1;
