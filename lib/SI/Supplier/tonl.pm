package SI::Supplier::tonl;

use strict;
use warnings;
use Log::Any qw( $log );
use LWP::UserAgent;
use JSON::XS qw( encode_json decode_json );
use parent 'SI::Supplier::base';

use Class::Tiny qw( username password ), {
    url => 'http://b2b.1091.ru/api/',
    ua  => sub { my $ua = LWP::UserAgent->new; },
    currency => 'RUB',
};

sub items_avail {
    my ( $self, %args ) = @_;

    my %req_products = map { $_ => undef } @{ $args{product_ids} || [] };
    my $response = $self->request;
    my $items    = decode_json( $response->content )->{Tovar};

    my %items_avail;
    ITEMS:
    for my $item ( @$items ) {
        next ITEMS if ( !$item->{Article} );
        next ITEMS if ( %req_products && !exists $req_products{ $item->{Article} } );

        $log->tracef( "Processing item %s", $item->{Article} );
        my $price = $item->{PriceMSK_RUR};
        $price   += $self->delivery_fee_for( $price );
        $items_avail{ $item->{Article} } = {
            item_id => $item->{Article},
            shipment_city => 'SkladMSK',
            cnt           => $item->{SkladMSK},
            price         => $price,
        };
    }

    return [ values %items_avail ];
}

sub request {
    my ( $self ) = @_;

    my $res;
    TRIES:
    for my $try ( 0 .. 2 ) {
        $log->tracef( 'Send request #%d %s', $try, $self->url );
        $res = $self->ua->post(
            $self->url,
            Content_Type => 'application/JSON',
            Content      => encode_json( {
                Login    => $self->username,
                Password => $self->password,
            })
        );

        last TRIES if ( $res->is_success );
    }

    if ( !$res->is_success ) {
        die sprintf( 'TradeOnline error: %s %s', $res->code, $res->content ); 
    }

    return $res;
}
1;
