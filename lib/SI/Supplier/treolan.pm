package SI::Supplier::treolan;

use strict;
use warnings;

use Log::Any qw( $log );
use SOAP::Lite;
use XML::Simple;
use Readonly;
no warnings 'XML::Simple';

use parent 'SI::Supplier::base';
use Class::Tiny qw( username password use_shipment_methods ), {
    currency => 'RUB',
    soap     => sub { $_[0]->_build_soap },
    wsdl_url => 'https://api.treolan.ru/ws/service.asmx?WSDL',
};

sub _build_soap {
    my $self = shift;

    my $soap = SOAP::Lite->service( $self->wsdl_url );
    $soap->on_fault( sub {
        $log->errorf( "Error: %s", $_[1]->body->{Fault}{faultstring} );
        die $_[1]->body->{Fault}{faultstring};
    } );

    return $soap;
}

sub items_avail {
    my ( $self, %args ) = @_;

    my %positions;
    my $xml_response = $self->soap->GenCatalogV2( $self->username, $self->password, '', 0, '', 0, 0, 0, 0, 1 );

    my $data  = XML::Simple::XMLin( $xml_response, KeyAttr => { position => '+articul' } );
    my @stack = ( $data );
    while ( my $node = shift @stack ) {
        if ( exists $node->{category} ) {
            if ( ref $node->{category} eq 'HASH' ) {
                push @stack, $node->{category};
            }
            elsif ( ref $node->{category} eq 'ARRAY' ) {
                for my $cat ( @{ $node->{category} } ) {
                    push @stack, $cat;
                }
            }
        }
        if ( exists $node->{position} ) {
            if ( $node->{position}{articul} ) {
                $positions{ $node->{position}{articul} } = $node->{position};
            }
            else {
                for my $articul_key ( keys %{ $node->{position} } ) {
                    $positions{ $articul_key } = $node->{position}{ $articul_key };
                }
            }
        }
        
    }

    my @items_avail = map {
        my $cnt = ( $_->{freenom} eq '0*' ) ? 0
                : ( $_->{freenom} )         ? 1
                :                             0;
        my $price = ( $_->{currency} eq 'RUB' ) ? $_->{price}
                  : ( $_->{currency} eq 'USD' ) ? sprintf( "%0.2f", $_->{price} * ( $self->usd_rate + ( $self->usd_rate * 0.024 ) ) )
                  :                               undef;
        $price += $self->delivery_fee_for( $price );
        +{
            item_id => $_->{articul},
            cnt     => $cnt,
            price   => $price,
        }
    } values %positions;

    if ( $args{product_ids} ) {
        my %lookup   = map { ( $_ => undef ) } @{ $args{product_ids} };
        @items_avail = grep { exists $lookup{ $_->{item_id} } } @items_avail;
    }

    return \@items_avail;
}

1;
