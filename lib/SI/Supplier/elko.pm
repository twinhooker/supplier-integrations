package SI::Supplier::elko;
use common::sense;
use Log::Any qw( $log );
use LWP::UserAgent;
use HTTP::Request;
use URI;
use JSON::XS qw( encode_json decode_json );
use List::MoreUtils qw( natatime );

use Readonly;

Readonly my $ITEMS_AVAIL_MAX_ITEM => 100;
use parent 'SI::Supplier::base';
use Class::Tiny qw( username password ), {
    url        => sub { URI->new( 'https://api.elko.ru/api/' ) },
    auth_token => '',
    ua         => sub { $_[0]->_build_ua },
    currency   => 'RUB',
};

sub BUILD {
    my $self = shift;
    $self->authorize;
}

sub _build_ua {
    my $self = shift;

    my $ua = LWP::UserAgent->new;
    $ua->ssl_opts( verify_hostname => 0 );

    return $ua;
}

sub items_avail {
    my ( $self, %args ) = @_;

    my $shipment_city = $args{shipment_info}[0];
    my $product_ids   = $args{product_ids};

    my $product_id_iter = natatime( $ITEMS_AVAIL_MAX_ITEM, @$product_ids );

    my %items_avail;
    while ( my @product_ids = $product_id_iter->() ) {
        my $res = $self->request(
            'GET'      => [qw( Catalogs AvailabilityAndPrice )],
            productIds => \@product_ids
        );

        my $elko_items = decode_json( $res->content );
        ITEMS:
        for my $elko_item ( @$elko_items ) {
            STOCK:
            for my $stock ( @{ $elko_item->{stockQuantity} } ) {
                my ( $quantity ) = $stock->{quantity} =~ m/(\d+)/msx;
                my $price = $elko_item->{price};
                $price   += $self->delivery_fee_for( $price );

                $items_avail{ $elko_item->{productId} } = {
                    item_id       => $elko_item->{productId},
                    shipment_city => $stock->{stock},
                    cnt           => $quantity,
                    price         => $price,
                };
            }
        }
    }

    return [ values %items_avail ];
}

sub request {
    my ( $self, $method, $path, %data ) = @_;

    my $uri  = $self->url->clone;
    my @path = $uri->path_segments;
    pop @path;
    $uri->path_segments( @path, @$path );

    my $headers = [
        'Content-Type'  => 'application/json; charset=UTF-8',
       ('Authorization' => sprintf( 'Bearer %s', $self->auth_token ) ) x!!  $self->auth_token
    ];

    my $req = HTTP::Request->new( $method, $uri, $headers );
    if ( $method eq 'POST' ) {
        $req->content( encode_json( \%data ) );
    }
    else {
        $uri->query_form( \%data );
        $req->uri( $uri );
    }

    my $res;
    TRIES:
    for my $try ( 0 .. 2 ) {
        # $log->infof( 'Send request #%d %s', $try, $uri );
        $res = $self->ua->request( $req );

        last TRIES if $res->is_success;
    }
    if ( ! $res->is_success ) {
        die sprintf( 'ELKO error: %s %s', $res->code, $res->content );
    }

    return $res;
}

sub authorize {
    my ( $self ) = @_;
    my $res = $self->request(
        POST => [qw( Token CreateToken )],
        username => $self->username,
        password => $self->password,
    );

    $self->auth_token( $res->content );

    return;
}
1;
