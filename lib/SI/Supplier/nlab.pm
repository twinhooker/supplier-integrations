package SI::Supplier::nlab;
use common::sense;
use Log::Any qw( $log );
use LWP::UserAgent;
use JSON::XS qw( encode_json decode_json );
use List::Util qw( any );
use Readonly;
use DDP;
use parent 'SI::Supplier::base';

use Class::Tiny {
    url        => sub { URI->new( 'http://services.netlab.ru/' ) },
    username   => '',
    password   => '',
    auth_token => '',
    ua         => sub { my $ua = LWP::UserAgent->new; },
    currency   => 'USD'
};

Readonly my @EXCLUDE_WORDS => (
    "Demo",
    "Демо",
    "Как новый",
    "Уценка",
    "Refabrished",
    "RFB",
    "Плохая упаковка",
);

sub BUILD {
    my $self = shift;
    $self->authorize;

}

# redefine usd rate
sub _build_usd_rate {
    my $self = shift;

    my $res  = $self->api_request( [qw( rest catalogsZip info.json )] );
    my $rate = $res->{entityListResponse}{data}{items}[0]{properties}{usdRateNonCash};

    return $rate;
}

sub items_avail {
    my ( $self, %args ) = @_;
    my %req_products = map { $_ => undef } @{ $args{product_ids} || [] };

    my %locations;
    my %items_avail;
    my @stack = ( [ 'В наличии' ] );
    while ( @stack ) {
        my $path   = shift @stack;
        my @path   = @$path;
        $path[-1]  = sprintf( '%s.json', $path[-1] );

        my $res      = $self->api_request( [qw( rest catalogsZip ), @path ] );
        my $category = $res->{catalogResponse}{data}{category};
        my $goods    = $res->{categoryResponse}{data}{goods};

        if ( $category && ref $category && ref $category eq 'ARRAY' ) {
            my @new_paths = map { [ @$path, $_->{id} ] } @$category;
            push @stack, @new_paths;
        }

        if ( $goods && ref $goods && ref $goods eq 'ARRAY' ) {
            ITEMS:
            for my $item ( @$goods ) {
                next ITEMS if ( !exists $req_products{ $item->{id} } );

                my $name = $item->{properties}{'название'};
                if ( any { $name =~ m/\Q$_/msx } @EXCLUDE_WORDS ) {
                    $log->tracef("Skipping item %s.", $name);
                    next ITEMS;
                }
                for my $key ( keys %{ $item->{properties} } ) {
                    if ( $key =~ m/количество/msx) {
                        $locations{ $key }++;                    
                    }
                }
                my @locations_cnt = map { $item->{properties}{ $_ } } @{ $args{shipment_info} };
                my ( $cnt )       = grep { $_ != 0 } @locations_cnt;

                my $price = $item->{properties}{'цена по категории F'};
                $price   += $self->delivery_fee_for( $price );

                $items_avail{ $item->{id} } = {
                    item_id       => $item->{id},
                    shipment_city => '',
                    cnt           => $cnt,
                    price         => $self->usd_rate * $price,
                }
            }
        }
    }
    return [ values %items_avail ];
}

sub authorize {
    my ( $self ) = @_;

    if ( !$self->auth_token ) {
        my $res = $self->request(
            GET      => [qw( rest authentication token.json )],
            username => $self->username,
            password => $self->password,
        );
        my @response = split '&&', $res->content;
        my $decoded_content = decode_json( $response[1] );
        $self->auth_token( $decoded_content->{tokenResponse}{data}{token} );
    }

    return;
}

sub api_request {
    my ( $self, $path ) = @_;

    my $res  = $self->request( 'GET' => $path );
    my @res  = split '&&', $res->content;
    my $data = decode_json( $res[1] );

    return $data;
}

sub request {
    my ( $self, $method, $path, %data ) = @_;

    my $uri  = $self->url->clone;
    my @path = $uri->path_segments;
    pop @path;
    $uri->path_segments( @path, @$path );

    my $headers = [ 'Content-Type' => 'application/json; charset=UTF-8' ];

    my $req = HTTP::Request->new( $method, $uri, $headers );
    if ( $method eq 'POST' ) {
        $req->content( encode_json( \%data ) );
    }
    else {
        $data{oauth_token} = $self->auth_token;
        $uri->query_form( \%data );
        $req->uri( $uri );
    }

    my $res;
    TRIES:
    for my $try ( 0 .. 2 ) {
        # $log->infof( 'Send request #%d %s', $try, $uri );
        $res = $self->ua->request( $req );

        last TRIES if $res->is_success;
    }
    if ( ! $res->is_success ) {
        die sprintf( 'NetlabClient error: %s %s', $res->code, $res->content );
    }

    return $res;
}

1;
