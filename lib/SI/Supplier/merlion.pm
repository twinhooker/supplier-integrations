package SI::Supplier::merlion;
use common::sense;
use Log::Any '$log';
use Readonly;
use Time::Piece;
use List::MoreUtils qw( natatime );
use SOAP::Lite; #  +trace => 'all';
use File::Fetch;

Readonly my $ITEM_AVAIL_MAX_ITEM         => 500;
Readonly my $ITEM_AVAIL_ONLY_AVAIL_FALSE => 0;
Readonly my $COMMAND_RESULT_SUCCESS      => 'Сделано';
Readonly my $COMMAND_RESULT_ERROR        => 'Ошибка';

use parent 'SI::Supplier::base';
use Class::Tiny qw( use_shipment_methods ), {
    soap_client         => sub { $_[0]->_build_soap_client },
    username            => sub { die "User name required" },
    password            => sub { die "Password required" },
    wsdl_url            => sub { die "WSDL url required." },
    proxy               => sub { die "SOAP proxy url required." },
    default_ns          => sub { die "Default ns url required." },
    image_url           => sub { die "Image url required." },
    image_download_path => sub { die "Image download path required." },
    image_prefix        => sub { '' },
    currency            => 'RUB',
};

sub BUILD {
    my ( $self, $args ) = @_;

    *SOAP::Transport::HTTP::Client::get_basic_credentials = sub {
        return $self->username => $self->password;
    };
}

sub _build_soap_client {
    my $self = shift;

    my $soap = SOAP::Lite->service( $self->wsdl_url );
    $soap->proxy( $self->proxy );
    $soap->default_ns( $self->default_ns );
    $soap->on_fault( sub {
        $log->errorf( "Error: %s", $_[1]->body->{Fault}{faultstring} );
        die $_[1]->body->{Fault}{faultstring};
    } );

    return $soap;
}

sub soap {
    my ( $self, %args ) = @_;

    my $seconds = $args{sleep} || 1;
    sleep $seconds;

    return $self->soap_client
}

sub items_avail {
    my ( $self, %args ) = @_;

    my %items_avail;
    my ( $product_ids, $shipment_info ) = @args{qw( product_ids shipment_info )};

    for my $shipment_method ( @{ $shipment_info } ) {

        my $pid_iterator = natatime( $ITEM_AVAIL_MAX_ITEM, @{ $product_ids } );
        while ( my @request_pids = $pid_iterator->() ) {

            my $avail_part = $self->_items_avail(
                shipment_method => $shipment_method,
                product_ids     => \@request_pids
            );

            for my $item_info ( @$avail_part ) {
                my $key = $item_info->{item_id} . $item_info->{shipment_code};
                $items_avail{ $key } = $item_info;
            }
        }
    }

    return [ values %items_avail ];
}

sub _items_avail {
    my ( $self, %args ) = @_;

    my $soap_array = SOAP::Data->name(
        'item_id' => \SOAP::Data->value(
            SOAP::Data->name('item' => @{ $args{product_ids} } )->type('string')
            )
    )->type('ArrayOfString');

    my $data = $self->soap->call(
        'getItemsAvail',
        SOAP::Data->name('cat_id')->value(''),
        SOAP::Data->name('shipment_method')->value( $args{shipment_method}{code} )->type('string'),
        SOAP::Data->name('shipment_date')->value( $args{shipment_method}{shipment_date} ),
        SOAP::Data->name('only_avail')->value( '1' ),
        $soap_array
    );

    my @result_array = ( ref $data->result->{item} eq 'ARRAY' )
        ? @{ $data->result->{item} }
        : ( $data->result->{item} );
    
    my @items_avail;
    for my $rv ( @result_array ) {
        my $cnt = 0;
        $log->tracef( "OnlineReserve:%s, AvailableClient:%s, AvailableClientRG:%s",
                      $rv->{Online_Reserve}, $rv->{AvailableClient}, $rv->{AvailableClient_RG} );
        if ( $rv->{Online_Reserve} eq '0' || $rv->{Online_Reserve} eq '2' ) {
            $cnt = $rv->{AvailableClient} || $rv->{AvailableClient_RG};
        }

        my $price = $rv->{PriceClientRUB};
        $price   += $self->delivery_fee_for( $price );
        push @items_avail, {
            item_id        => $rv->{No},
            cnt            => $cnt,
            price          => $price,
            shipment_code  => $args{shipment_method}{code},
        };
    }

    return \@items_avail;
}

#
# Orders
#

sub create_order {
    my ( $self, %args ) = @_;

    $log->infof('Creating new order ShipmentCode:%s, ShipmentDate:%s.', @args{qw( shipment_code shipment_date )});
    my $command_id = $self->soap->setOrderHeaderCommand( undef, $args{shipment_code}, $args{shipment_date} );
    my $command    = $self->wait_command( $command_id );
    my $order_res  = $self->soap->getOrdersList( $command->{document_no} );
    my $order      = $order_res->{item};

    return $order;
}

sub get_order {
    my ( $self, %args ) = @_;

    $log->infof('Order lookup ShipmentCode:%s, ShipmentDate:%s.', @args{qw( shipment_code shipment_date )});
    my %order;
    my $res = $self->soap->getOrdersList('');
    my $merlion_orders = ( ref $res->{item} eq 'ARRAY' )       ? $res->{item}
                       : ( defined $res->{item}{document_no} ) ? [ $res->{item} ]
                       :                                         [];
    sleep( 2 );

    ORDERS:
    for my $order ( @{ $merlion_orders } ) {
        if (    $order->{ShipmentMethodCode} eq $args{shipment_code}
             && $order->{Status}             eq '-'
        ) {
            %order = %$order;
            last ORDERS;
        }
    }

    return \%order;
}

sub push_order_line {
    my ( $self, $order, $item_line ) = @_;

    $log->infof(
        'Order new merlion item order_id:%s product_id: %s qty: %d.',
        $order->{document_no}, $item_line->{foreign_product_id}, $item_line->{count}
    );

    my $command_id;
    my $order_line = $self->get_order_line( $order, $item_line );
    if ( $order_line ) {
        $command_id = $self->soap->setAddOrderLineCommand(
            $order->{document_no},
            $item_line->{foreign_product_id},
            $item_line->{count}
        );
    }
    else {
        $command_id = $self->soap->setOrderLineCommand(
            $order->{document_no},
            $item_line->{foreign_product_id},
            $item_line->{count}
        );
    }
    my $command = $self->wait_command( $command_id );

    return $command;
}

sub get_order_line {
    my ( $self, $order, $line ) = @_;

    my $res = $self->soap->getOrderLines( $order->{document_no} );
    my $order_lines = ( ref $res->{item} eq 'ARRAY' )   ? $res->{item}
                    : ( defined $res->{item}{item_no} ) ? [ $res->{item} ]
                    :                                     [];

    my $existing_line;
    ORDER_LINES:
    for my $order_line ( @$order_lines ) {
        if ( $order_line->{item_no} eq $line->{foreign_product_id} ) {
            $existing_line = $order_line;
            last ORDER_LINES;
        }
    }

    return $existing_line;
}

#
# Descriptions
#

sub get_item_images {
    my ( $self, $item_id ) = @_;

    my $item_ids = SOAP::Data->name(
            'item_id' => \SOAP::Data->value(
                SOAP::Data->name( item => $item_id )->type('string')
                )
        )->type('ArrayOfString');

    my $res = $self->soap->call(
        'getItemsImages',
        SOAP::Data->name('cat_id')->value(''),
        $item_ids,
    );

    my %images;

    if ( ref $res->result->{item} eq 'ARRAY' ) {
        IMAGES:
        for my $image ( @{ $res->result->{item} } ) {
            next IMAGES if ( $image->{ViewType} ne 'v' );

            $images{pic_big} ||= $image
                if ( $image->{SizeType} eq 'b' );

            $images{pic_small} ||= $image
                if ( $image->{SizeType} eq 's' );

            $images{pic_medium} ||= $image
                if ( $image->{SizeType} eq 'm' );
        }
        $images{pic_big} = $images{pic_medium}
            if ( !$images{pic_big} && $images{pic_medium} );
    }

    return \%images;
}

sub download {
    my ( $self, $image ) = @_;

    $log->tracef( 'Downloading image: %s', $image );
    my $image_name = $image->{FileName};
    my $uri        = $self->image_url . $image_name;

    $log->tracef( 'Download from %s to %s', $uri, $self->image_download_path );
    my $ff         = File::Fetch->new( uri => $uri );
    my $where      = $ff->fetch( to => $self->image_download_path ) or die $uri . $ff->error;

    return $self->image_prefix . $image_name;
}

sub get_item_property_sections {
    my $self = shift;

    my $sections_res = $self->soap->getItemsPropertiesSections('');
    my %sections_map = map { ( $_->{id} => $_->{name} ) } @{ $sections_res->{item} };

    return \%sections_map;
}

sub get_items_properties {
    my ( $self, %args ) = @_;

    my $page = 0;
    my $items_on_page = 5000;
    my $MAX_ITEMS_PER_REQUEST = 500;
    my @properties;

    my @item_ids = splice( @{ $args{item_id} }, 0, $MAX_ITEMS_PER_REQUEST );

    ITEMS_PROPERTIES:
    while (1) {
        last ITEMS_PROPERTIES if ( !@item_ids );

        my $item_ids = SOAP::Data->name(
            'item_id' => \SOAP::Data->value(
                SOAP::Data->name( item => @item_ids )->type('string')
                )
        )->type('ArrayOfString');

        my $response = $self->soap( sleep => 1 )->call(
            'getItemsProperties',
            SOAP::Data->name('cat_id')->value(''),
            $item_ids,
            SOAP::Data->name('page')->value( $page ),
            SOAP::Data->name('rows_on_page')->value( $items_on_page ),
        );

        my $items = $response->result->{item};

        if ( ref $items eq 'ARRAY' ) {
            push @properties, @$items;
        }
        last ITEMS_PROPERTIES
            if ( ref $items ne 'ARRAY' || scalar @$items < $MAX_ITEMS_PER_REQUEST );

        $page++;
    }

    return \@properties;
}

#
# Utils wait command
#

sub wait_command {
    my ( $self, $id ) = @_;

    die 'No command ID' if ( !$id );

    my %command_result = ( status => 2 );
    my $tries = 30;

    WAIT_COMMAND:
    while ( $tries-- ) {
        my $res_item = $self->soap->getCommandResult( $id )->{item};
        sleep( 1 );
        next WAIT_COMMAND if ( !defined $res_item->{ProcessingResult} );

        my $status = ( $res_item->{ProcessingResult} eq $COMMAND_RESULT_SUCCESS ) ? 1
                   : ( $res_item->{ProcessingResult} eq $COMMAND_RESULT_ERROR )   ? 2
                   :                                                                undef;

        if ( $status ) {
            @command_result{qw( document_no status )} = ( $res_item->{DocumentNo}, $status );
            if ( $command_result{status} == 2 ) {
                $log->errorf( 'MerlionClient got error from seriver: %s', $res_item->{ErrorText} );
            }
            last WAIT_COMMAND;
        }
    }

    return \%command_result;
}

#
# Dictionaries
#

sub shipment_methods {
    my $self = shift;

    my $today = localtime->ymd;
    my $merlion_shipment_methods = $self->soap->getShipmentMethods( '' );
    my @selected_methods = grep {
        exists $self->use_shipment_methods->{ $_->{Code} }
    } @{ $merlion_shipment_methods->{item} };

    my @shipment_methods;
    for my $sm ( @selected_methods ) {
        my $dates_rv = $self->soap->getShipmentDates( '', $sm->{Code} );
        my ( $date ) = sort map {  $_->{Date} } @{ $dates_rv->{item} };

        push @shipment_methods, {
            code          => $sm->{Code},
            shipment_date => $date,
            date          => $today,
        };
        sleep( 11 );
    }

    return \@shipment_methods;
}


1;
