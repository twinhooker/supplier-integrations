package SI::Supplier::ocs;
use common::sense;
use LWP::UserAgent;
use HTTP::Request::Common;
use JSON::XS qw( encode_json decode_json );
use utf8;
use Log::Any '$log';
use URI;

use parent 'SI::Supplier::base';
use Class::Tiny qw(), {
    url      => 'https://connector.b2b.ocs.ru/api/v2',
    token    => '',
    ua       => sub { LWP::UserAgent->new },
    currency => 'RUB',
};

sub items_avail {
    my ( $self, %args ) = @_;

    my $shipment_city = $args{shipment_info}[0];
    my $product_ids   = $args{product_ids};
    my $items_avail   = $self->_get_items_avail( $product_ids, $shipment_city );

    my %items_avail = map {
        ( $_ => { item_id => $_, cnt => 0, shipment_city => $shipment_city, price => undef } )
    } @$product_ids;

    OCS_ITEMS:
    for my $ocs_item ( @$items_avail ) {
        my $item = $items_avail{ $ocs_item->{product}{itemId} };
        # Allways update price
        $item->{price} = $ocs_item->{price}{order}{value};
        $item->{price} += $self->delivery_fee_for( $item->{price} );
        if ( @{ $ocs_item->{locations} } ) {
            LOCATIONS:
            for my $loc ( @{ $ocs_item->{locations} } ) {
                my $qty = $loc->{quantity}{value};
                if ( $loc->{canReserve} && $qty ) {
                    $item->{cnt} = $qty;
                }
            }
        }
    }
    return [ values %items_avail ];
}

sub _get_items_avail {
    my ( $self, $product_ids, $shipment_city ) = @_;

    my $url = URI->new( $self->url );
    $url->path_segments( $url->path_segments, qw( catalog products batch ) );
    $url->query_form(
        shipmentcity   => $shipment_city,
        includemissing => 'true',
        includesale    => 'true',
    );
    $log->infof('Loading %d products from OCS.', scalar @$product_ids);
    my $res = $self->ua->request(
        POST $url,
        'Content_Type' => 'application/json; charset=utf-8',
        'accept'       => 'application/json',
        'X-API-Key'    => $self->token,
        'Content'      => encode_json( $product_ids ), # [ '1000581105', '1000581105', '1000582338' ] ) #
    );
    if ( !$res->is_success ) {
        use Encode qw( decode_utf8 );
        $log->error( decode_utf8( $res->content ));
        return [];
    }
    my $products = decode_json( $res->content )->{result};
    $log->infof('Got %d products from OCS.', scalar @$products );

    return $products;
}

1;
