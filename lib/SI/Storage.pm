package SI::Storage;
use common::sense;

use DBI;
use List::Util qw( any );
use Log::Any '$log';
use Readonly;
use SQL::Abstract::Plugin::InsertMulti;
use Time::Piece;

Readonly my $MAX_ABSENT => 7;

use Class::Tiny qw( dsn username password ), {
    dbh => sub { $_[0]->_build_dbh },
    sql => sub { SQL::Abstract::More->new },
};

sub _build_dbh {
    my $self = shift;

    my $dbh = DBI->connect( $self->dsn, $self->username, $self->password, {
        mysql_enable_utf8 => 1,
        RaiseError        => 1,
        AutoCommit        => 1,
    } );

    return $dbh;
}

sub avail_info {
    my ( $self, %args ) = @_;
    my $api_suppliers = $args{api_suppliers};

    my @join = (
        -join => 'phpshop_products|p',
        {
            operator => '<=>',
            condition => { 'p.category' => { '=' => { -ident => 'c.id' } } },
        },
        'phpshop_categories|c',
    );

    my @columns = (
        'distinct( p.id )', 'p.parent_enabled', 'p.parent',
        'p.ignore_api|p_ignore_api', 'c.ignore_api', 'p.temp_ignore_api',
        'p.supplier', 'p.p_enabled', 'sklad', 'p.enabled',
        'start_price', 'p.price', 'c.min_price_margin'
    );

    my @qty_aggregator;
    my @suppliers;

    for my $supplier ( @$api_suppliers ) {
        my $table_name    = sprintf( 'si_%s_avail', $supplier );
        my $vid_key       = sprintf( 'p.%s_vid', $supplier );
        my $join_item_key = sprintf( '%s.%s', $table_name, 'item_id' );
        my $cnt_str       = sprintf( '%s.cnt', $table_name );
        my $qty_str       = sprintf( 'nullif(%s.cnt, 0)', $table_name );
        my $qty_col       = sprintf( 'coalesce(%s.cnt, 0)|%s_qty', $table_name, $supplier );
        my $formula_col   = sprintf( 'c.parser_formula_%s', $supplier );
        my $bonus_col     = sprintf( 'p.%s_bonus', $supplier );
        my $price_col     = sprintf( '%s.price|%s_in_price', $table_name, $supplier );

        push @join, {
            operator  => '=>',
            condition => {
                $vid_key => { '=' => { -ident => $join_item_key } },
                $cnt_str => { '>' => 0 }
            }
        },
        $table_name;

        push @qty_aggregator, $qty_str;
        push @columns, $qty_col, $formula_col, $bonus_col, $price_col;
        push @suppliers, $supplier;
    }

    my $csv_suppliers = $self->get_csv_suppliers;
    for my $csv_supplier ( @$csv_suppliers ) {
        my $table_name    = sprintf( 'si_csv_avail|%s', $csv_supplier );
        my $vid_key       = sprintf( 'p.%s_vid', $csv_supplier );
        my $join_item_key = sprintf( '%s.%s', $csv_supplier, 'item_id' );
        my $qty_str       = sprintf( 'nullif(%s.cnt, 0)', $csv_supplier );
        my $column_str    = sprintf( 'coalesce(%s.cnt, 0)|%s_qty', $csv_supplier, $csv_supplier );
        my $formula_col   = sprintf( 'c.parser_formula_%s', $csv_supplier );
        my $bonus_col     = sprintf( 'p.%s_bonus', $csv_supplier );
        my $price_col     = sprintf( '%s.price|%s_in_price', $csv_supplier, $csv_supplier );
        my $rrp_col       = sprintf( '%s.rrp|%s_rrp', $csv_supplier, $csv_supplier );

        push @join, {
            operator => '=>',
            condition => {
                $vid_key                 => { '=' => { -ident => $join_item_key } },
                "$csv_supplier.supplier" => $csv_supplier,
            }
        },
        $table_name;

        push @qty_aggregator, $qty_str;
        push @columns, $column_str, $formula_col, $bonus_col, $price_col, $rrp_col;
        push @suppliers, $csv_supplier;
    }
    my $qty = sprintf( 'coalesce( %s, 0 )|qty', join( ',', @qty_aggregator ) );
    push @columns, $qty;
    my $ignore_api =  $args{'c.ignore_api'} || { -in => [ 0, 1 ] };
    # by default check temp_ignore_api
    my $check_temp_ignore_api = ($args{dont_exclude_temp_ignore_api}) ? 0 : 1;
    my @where = (
        -and => [
            'c.ignore_api' => $ignore_api,
            ('p.temp_ignore_api' => { '<' => \'now()' }) x!! $check_temp_ignore_api,
            ('p.supplier'  => { -in => \@suppliers })    x!!! $args{'p.id'},
            ('p.id'        => $args{'p.id'})             x!!  $args{'p.id'},
            -or => [
                'p.parent_enabled' => { '=' => 1 },
                [
                    -and => [
                        'p.parent_enabled' => { '=' => 0 },
                        -or => [
                            'p.parent' => undef,
                            'p.parent' => '',
                        ]
                    ]
                ]
            ]
        ]
    );

    my @order_by = qw( p.id );
    push @order_by, '-si_merlion_avail.cnt' if ( any { $_ eq 'merlion' } @$api_suppliers );

    my ( $stmt, @bind ) = $self->sql->select(
        -columns  => \@columns,
        -from     => \@join,
        -group_by => [qw( p.id )],
        -order_by => \@order_by,
        -where    => \@where,
    );
    my $avail = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

    return $avail;
}

sub get_csv_suppliers {
    my $self = shift;

    my ( $stmt, @bind ) = $self->sql->select(
        -columns  => [qw(supplier)],
        -from     => [
            -join => 'si_csv_avail|si',
            {
                operator => '<=>',
                condition => { 'si.supplier' => { '=' => { -ident => 'ps.supplier_field' } } }
            },
            'phpshop_supplier|ps'
        ],
        -group_by => [qw(supplier)],
    );
    my $suppliers = $self->dbh->selectcol_arrayref( $stmt, undef, @bind );

    return $suppliers;
}

sub items_avail {
    my ( $self, $supplier, $item_id ) = @_;

    my $table_name = sprintf( 'si_%s_avail', $supplier );
    my %where      = ( item_id => $item_id );

    my ( $stmt, @bind ) = $self->sql->select( -from => $table_name, -where => \%where );
    my $avail_info = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

    return $avail_info;
}

sub set_supplier_avail {
    my ( $self, $supplier, $items_avail ) = @_;

    $log->infof( "Replacing %d items avail for %s.", scalar @$items_avail, $supplier );
    my $table = sprintf( 'si_%s_avail', $supplier );

    if ( $supplier eq 'nlab' ) {
        $items_avail = $self->delay_remove( $items_avail );
    }

    eval {
        $self->dbh->{AutoCommit} = 0;

        my ( $del_stmt, @bind ) = $self->sql->delete( $table );
        $self->dbh->do( $del_stmt );

        my ( $ins_stmt, @ins_bind ) = $self->sql->insert_multi( $table, $items_avail );
        $self->dbh->do( $ins_stmt, undef, @ins_bind );
        $self->dbh->commit;
    };
    if ( $@ ) {
        $log->errorf( 'DB error: %s.', $@ );
        eval { $self->dbh->rollback };
    }

    $self->dbh->{AutoCommit} = 1;

    return;
}

sub delay_remove {
    my ( $self, $current_avail ) = @_;

    my @real_avail;
    my %current_avail  = map { $_->{item_id} => $_ } @$current_avail;
    my $previous_avail = $self->dbh->selectall_arrayref('select * from si_nlab_avail', { Slice => {} });
    ITEMS:
    for my $old_item ( @$previous_avail ) {
        my $current_item = delete $current_avail{ $old_item->{item_id} };
        if ( !$current_item || $current_item->{cnt} == 0 ) {
            if ( !$current_item ) {
                $log->infof( "item(%s) not exists. absent is: %d.", $old_item->{item_id}, $old_item->{absent} );
            } else {
                $log->infof( "item(%s) exists but cnt = 0 setting to 1. absent is: %d.", $old_item->{item_id}, $old_item->{absent} );
                # $old_item->{cnt} = 1;
            }
            $old_item->{absent}++;
            next ITEMS if ( $old_item->{absent} >= $MAX_ABSENT );
            push @real_avail, $old_item;
        }
        else {
            $current_item->{absent} = 0;
            push @real_avail, $current_item;
        }
    }
    push @real_avail, values %current_avail;

    return \@real_avail;
}

sub get_shipment_info {
    my ( $self, $supplier ) = @_;

    my %info_for = (
        merlion => sub {
            my $today = localtime->ymd;
            my ( $stmt, @bind )  = $self->sql->select( 'si_merlion_shipmentmethods', '*', { date => { '>=' => $today } } );
            my $shipment_methods = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

            return $shipment_methods;
        },
        ocs => sub {

        }
    );
    my $shipment_info = $info_for{ $supplier }->();

    return $shipment_info;
}

sub set_shipment_info {
    my ( $self, $supplier, $shipment_info ) = @_;

    if ( $supplier eq 'merlion' ) {
        $self->dbh->do( 'delete from si_merlion_shipmentmethods' );

        for my $method ( @$shipment_info ) {
            my ( $stmt, @bind ) = $self->sql->insert( -into => 'si_merlion_shipmentmethods', -values => $method );
            $log->infof( 'Add new shipment method( Code: %s Date:%s )', @{ $method }{qw( code shipment_date )} );
            $self->dbh->do( $stmt, undef, @bind );
        }
    }

    return;
}

#
# Orders
#

sub add_order {
    my ( $self, $order ) = @_;

    my ( $stmt, @bind ) = $self->sql->insert( 'si_orders', $order );
    $log->infof(
        'Added new order to send queue( order_id:%d product_id:%d qty:%d supplier:%s shipment: %s foreign_id: %s )',
        @{ $order }{qw( order_id product_id count supplier shipment_code foreign_product_id )}
    );

    $self->dbh->do( $stmt, undef, @bind );

    return;
}

sub get_orders_lines {
    my ( $self ) = @_;

    my ( $stmt, @bind ) = $self->sql->select( 'si_orders', '*', { status => 0 } );
    my $orders_lines = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

    return $orders_lines;
}

sub set_order_line {
    my ( $self, $line, $command_res ) = @_;

    my ( $stmt, @bind ) = $self->sql->update(
        'si_orders',
        { status => $command_res->{status} },
        {
            order_id   => $line->{order_id},
            product_id => $line->{product_id}
        }
    );
    $self->dbh->do( $stmt, undef, @bind );

    return;
}

sub is_product_ordered {
    my ( $self, $order_id, $product_id, $supplier ) = @_;

    my ( $stmt, @bind ) = $self->sql->select(
        -columns => '*',
        -from    => 'si_orders',
        -where   => {
            order_id   => $order_id,
            product_id => $product_id,
            supplier   => $supplier,
        }
    );

    my @rv = $self->dbh->selectrow_array( $stmt, { Slice => {} }, @bind );

    return ( @rv ) ? 1 : 0;
}


1;
