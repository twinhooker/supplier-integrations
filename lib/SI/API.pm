package SI::API;
use strict;
use warnings;

use DBI;
use Config::General;
use SI::ShopClient;
use SI::Synchronizer;
use SI::Storage;
use Module::Load;
use Module::Load::Conditional qw(check_install);

use Log::Log4perl qw( :easy );
use Log::Any::Adapter;
use String::Flogger;
use Log::Any '$log';

use Class::Tiny qw( config_path log_path ), {
    log_level => $INFO,
    shop      => sub { $_[0]->_build_shop },
    config    => sub { $_[0]->_build_config },
    suppliers => sub { {} },
    storage   => sub { $_[0]->_build_storage },
};

sub BUILD {
    my ( $self, $args ) = @_;
    Log::Log4perl->easy_init({
        level  => $self->log_level,
        utf8   => 1,
        layout => '[%p]%d: %F{1}-%L-%M: %m%n',
        ($self->log_path) ? (file   => '>>'.$self->log_path ) : ()
    });

    Log::Any::Adapter->set('Log4perl');
}

sub _build_storage {
    my $self = shift;

    my $storage = SI::Storage->new(
        %{ $self->config->{storage} }{qw( dsn username password )},
        supplier_conf => $self->config->{supplier},
    );

    return $storage;
}

sub _build_shop {
    my $self = shift;

    my $shop_client = SI::ShopClient->new( %{ $self->config->{shopDB} }{qw( dsn username password )} );

    return $shop_client;
}

sub supplier {
    my ( $self, $supplier_name ) = @_;

    return $self->suppliers->{ $supplier_name }
        if ( exists $self->suppliers->{ $supplier_name } );
    my $supplier_module = sprintf( 'SI::Supplier::%s', $supplier_name );
    if ( !check_install(module => $supplier_module => undef) ) {
        $log->errorf("$supplier_module is not API supplier.");
        return undef;
    }
    load $supplier_module;

    my $db_supplier = $self->shop->get_suppliers( supplier_field => $supplier_name )->[0];
    $self->suppliers->{ $supplier_name } ||=
        $supplier_module->new(
            %{ $self->config->{supplier}{ $supplier_name } },
            username => $db_supplier->{api_login},
            password => $db_supplier->{api_password},
            delivery_price               => $db_supplier->{delivery_price},
            delivery_currency            => $db_supplier->{delivery_currency},
            delivery_second_price_border => $db_supplier->{delivery_second_price_border},
            delivery_second_price        => $db_supplier->{delivery_second_price},
        );

    return $self->suppliers->{ $supplier_name };
}

sub _build_config {
    my $self = shift;

    my $config = Config::General->new(
        -ConfigFile => $self->config_path,
        -UTF8 => 1,
        -SplitDelimiter => '\s*=\s*'
    );

    return { $config->getall };
}

sub shipment_info {
    my ( $self, $supplier ) = @_;

    my %shipment_info_of = (
        merlion => sub {
            my $sms = $self->storage->get_shipment_info( $supplier );
            if ( !@$sms ) {
                my $new_sms = $self->supplier( $supplier )->shipment_methods;
                $self->storage->set_shipment_info( $supplier, $new_sms );
                $sms = $self->storage->get_shipment_info( $supplier );
            }
            return $sms;
        },
        ocs => sub {
            return [ keys %{ $self->config->{supplier}{ocs}{shipment} } ];
        },
        marvel => sub {
            return [ keys %{ $self->config->{supplier}{marvel}{shipment} } ];
        },
        elko => sub {
            return [ keys %{ $self->config->{supplier}{marvel}{shipment} } ];
        },
        treolan => sub { },
        tonl    => sub {  },
        nlab    => sub { [ keys %{ $self->config->{supplier}{nlab}{shipment} } ] },
        # nlab    => sub { [ "количество на Лобненской" ] },
    );

    my $sms = $shipment_info_of{ $supplier }->();

    return $sms;
}

1;
