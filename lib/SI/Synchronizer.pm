package SI::Synchronizer;
use common::sense;
use warnings;
use parent qw( SI::API );

use Math::Calc::Parser qw( calc );
use List::Util qw( reduce first uniq );
use Log::Any '$log';
use Readonly;

Readonly my $SKLAD_ENABLED  => '0';
Readonly my $SKLAD_DISABLED => '1';

Readonly my $PRODUCT_ENABLED  => '1';
Readonly my $PRODUCT_DISABLED => '0';

Readonly my $IGNORE_API_TURN_ONOFF => 0;
Readonly my $IGNORE_API_TURN_ON    => 1;
Readonly my $IGNORE_API_SKIP       => 2;


sub sync_avail {
    my ( $self, $supplier ) = @_;

    my $supplier_id_key = sprintf( '%s_vid', $supplier );
    my $products = $self->shop->get_products(
        $supplier_id_key => { '!=' => [ -and => ( '', undef ) ] }
    );
    my %products_map = map {( $_->{ $supplier_id_key } => $_ )} @$products;
    $log->infof( 'Going to ask %d products from %s.', scalar @$products, $supplier );
    my $shipment_to  = $self->shipment_info( $supplier );
    my $supplier_obj = $self->supplier( $supplier );
    return undef if ( !$supplier_obj );

    my $items_avail = $supplier_obj->items_avail(
        product_ids   => [ keys %products_map ],
        shipment_info => $shipment_to,
    );
    $self->storage->set_supplier_avail( $supplier, $items_avail );

    return;
}

sub update_availability {
    my ( $self, %args ) = @_;

    my @api_suppliers;
    my %default_product_status;
    my $suppliers = $self->shop->get_suppliers;
    for my $supplier ( @$suppliers ) {
        if ($supplier->{api_enable} && $supplier->{has_api}) {
            push @api_suppliers, $supplier->{supplier_field};
        }
        $default_product_status{ $supplier->{supplier_field} } = $supplier->{product_status};
    }
    my $csv_suppliers = $self->storage->get_csv_suppliers;
    my $lookup_suppliers = [ @$csv_suppliers, @api_suppliers ];

    my $info = $self->storage->avail_info(api_suppliers => \@api_suppliers);
    my @avail_ids;
    my @not_avail_ids;

    AVAIL_INFO:
    for my $avail_info ( @$info ) {
        my $ignore_api
            = ( $avail_info->{p_ignore_api} == 3 ) ? $avail_info->{ignore_api}
            :                                        $avail_info->{p_ignore_api};
        next AVAIL_INFO if ( $ignore_api == $IGNORE_API_SKIP );

        $log->tracef( "Processing: %s", $avail_info );
        my $best_supplier = $self->_best_supplier( $lookup_suppliers, $avail_info );
        if ( $best_supplier ) {
            my %update_data;
            if ( $best_supplier->{price} != $avail_info->{start_price} ) {
                $log->infof( 'Product(%d) Got new start_price %s -> %s.', $avail_info->{id}, $avail_info->{start_price}, $best_supplier->{price} );
                $avail_info->{start_price} = $best_supplier->{price};
                $update_data{start_price}  = $best_supplier->{price};
            }

            # update supplier
            if ( $best_supplier->{supplier} ne $avail_info->{supplier} ) {
                $log->infof( 'Product(%d) Got new supplier %s -> %s.', $avail_info->{id}, $avail_info->{supplier}, $best_supplier->{supplier} );
                $update_data{supplier}  = $best_supplier->{supplier};
                $avail_info->{supplier} = $best_supplier->{supplier};
            }
            if ( scalar keys %update_data ) {
                $self->shop->set_product({ id => $avail_info->{id}, %update_data });
            }
            # 
            $self->check_price_margin( $avail_info );

            # update availability
            if (    $avail_info->{sklad}   eq $SKLAD_DISABLED
                 || $avail_info->{enabled} eq $PRODUCT_DISABLED
            ) {
                my $product = $self->shop->get_products( id => $avail_info->{id} )->[0];
                $self->shop->enable_product( $product, $default_product_status{ $product->{supplier} } );
                push @avail_ids, $avail_info->{id};
            }
        }
        elsif (   $avail_info->{sklad} eq $SKLAD_ENABLED
               && $ignore_api == $IGNORE_API_TURN_ONOFF
        ) {
            $log->infof( 'Product(%d) No supplier found. Disabling', $avail_info->{id} );
            push @not_avail_ids, $avail_info->{id};
        }
    }

    $log->infof( "New not in stock: %d", scalar @not_avail_ids );
    $log->infof( "New Avail: %d", scalar @avail_ids );

    if ( @not_avail_ids ) {
        my $not_avail_products = $self->shop->get_products( id => { -in => \@not_avail_ids } );
        $self->shop->disable_product( $_ ) for ( @$not_avail_products );
    }

    return;
}

sub check_price_margin {
    my ( $self, $avail_info ) = @_;

    my $rrp_col = sprintf( '%s_rrp', $avail_info->{supplier} );
    # check price
    if ( $avail_info->{ $rrp_col } && $avail_info->{price} != $avail_info->{ $rrp_col } ) {
        # rrp has only csv suppliers so if there is a row, no need to check qty
        $log->infof( 'Supplier %s has recommended retail price %s setting it.', $avail_info->{supplier}, $avail_info->{ $rrp_col } );

        $self->shop->set_product({
            id    => $avail_info->{id},
            price => $avail_info->{ $rrp_col }
        });
    }
    elsif ( $avail_info->{price} - $avail_info->{start_price} < $avail_info->{min_price_margin} ) {
        my $markup     = $avail_info->{start_price} * 0.02;
        $markup        = $avail_info->{min_price_margin} if ( $markup < $avail_info->{min_price_margin} );
        my $new_price  = int( $avail_info->{start_price} + $markup );
        $new_price    += ( 10 - ( $new_price % 10 ) );

        $log->infof( 'Product(%d) existing price is too low, updating: %s -> %s.', $avail_info->{id}, $avail_info->{price}, $new_price );
        $self->shop->set_product({
            id    => $avail_info->{id},
            price => $new_price
        });
    }

    return;
}

sub check_sklad_products {
    my ( $self ) = @_;
    my $manual_suppliers = $self->shop->get_suppliers( supplier_manual => 1 );
    my @supplier_codes   = map { $_->{supplier_field} } @$manual_suppliers;

    my $products      = $self->shop->get_products( supplier => { -in => \@supplier_codes } );
    my @api_suppliers = map { $_->{supplier_field} } @{$self->shop->get_suppliers( has_api => 1, api_enable => 1 )};
    my $csv_suppliers = $self->storage->get_csv_suppliers;
    my $suppliers     = [ @api_suppliers, @$csv_suppliers ];

    my %default_product_status = map { $_->{supplier_field} => $_->{product_status} } @{$self->shop->get_suppliers};

    MANUAL_SUPPLIER_PRODUCTS:
    for my $product ( @$products ) {
        my $avail_info = $self->storage->avail_info(
            'p.id'          => $product->{id},
            'api_suppliers' => \@api_suppliers
        )->[0];
        $log->tracef( 'Processing: %s', $avail_info );

        my $best_supplier = $self->_best_supplier( $suppliers, $avail_info );
        if (    $best_supplier
             && (    $avail_info->{sklad} eq $SKLAD_DISABLED
                  || $avail_info->{start_price} > $best_supplier->{price}
             )
        ) {
            # change supplier and set to work with api
            $self->shop->set_product({
                id          => $avail_info->{id},
                supplier    => $best_supplier->{supplier},
                start_price => $best_supplier->{price},
                ignore_api  => $IGNORE_API_TURN_ONOFF,
            });
            $avail_info->{start_price} = $best_supplier->{price};
            $avail_info->{supplier}    = $best_supplier->{supplier};
            $self->check_price_margin( $avail_info );
            $self->shop->enable_product( $product, $default_product_status{ $best_supplier->{supplier} } );
        }
    }

    return;
}

sub _best_supplier {
    my ( $self, $suppliers, $avail_info ) = @_;
    $log->tracef("Get best supplier for product(%d) with suppliers: %s", $avail_info->{id}, $suppliers);
    my $parents = [];
    if ( $avail_info->{parent_enabled} && $avail_info->{parent} ) {
        my @ids = split( ',', $avail_info->{parent} );
        $log->tracef( 'Load parent(%s) data for product(%d) to check bonus.', \@ids, $avail_info->{id} );
        $parents = $self->shop->get_products( id => { -in => \@ids } );
    }

    my @suppliers;
    SUPPLIER:
    for my $supplier ( @$suppliers ) {
        my $qty_col     = sprintf( '%s_qty', $supplier );
        my $formula_col = sprintf( 'parser_formula_%s', $supplier );
        my $bonus_col   = sprintf( '%s_bonus', $supplier );
        my $price_col   = sprintf( '%s_in_price', $supplier );

        next SUPPLIER if ( !$avail_info->{ $formula_col } );
        # next SUPPLIER if ( !$avail_info->{ $qty_col } );

        if ( $avail_info->{ $qty_col } > 0 ) {
            my $formula      =  $avail_info->{ $formula_col };
            $formula         =~ s/\,/\./mgsx;
            my $multiplier   =  ( $formula ) ? calc( $formula ) : 1;
            my $parent_bonus =  first { defined( $_ ) } map { $_->{ $bonus_col } } @$parents;

            my $bonus      = $avail_info->{ $bonus_col } || $parent_bonus || 0;
            my $out_price  = $avail_info->{ $price_col } * $multiplier - $bonus;
            $log->tracef( 'Count product(%d) in price %s = %s*%s-%s',
                          $avail_info->{id}, $out_price, $avail_info->{ $price_col }, $multiplier, $bonus );
            push @suppliers, {
                supplier => $supplier,
                in_price => $avail_info->{ $price_col },
                price    => int( $out_price ),
                qty      => $avail_info->{ $qty_col },
            };
        }
    }
    $log->tracef( 'Suppliers found %s', \@suppliers );
    my $best_price = reduce {
        ( $a->{price} == $b->{price} && $a->{supplier} eq $avail_info->{supplier} ) ? $a
      : ( $a->{price} < $b->{price} )                                               ? $a
      :                                                                               $b;
    } grep { $_->{price} } @suppliers;

    return $best_price;
}

sub sync_orders {
    my ( $self, $supplier ) = @_;

    my $orders = $self->shop->new_orders;
    NEW_ORDERS:
    for my $order ( @$orders ) {
        my $cart = $order->{orders}{Cart}{cart};
        my @product_ids = keys %{ $cart };

        PRODUCTS:
        for my $product_id ( @product_ids ) {
            my $is_ordered = $self->storage->is_product_ordered( $order->{id}, $product_id, $supplier );
            next PRODUCTS if ( $is_ordered );

            my $product = $self->shop->get_products( id => $product_id )->[0];
            next PRODUCTS if ( !$product || $product->{supplier} ne $supplier );

            $log->tracef( 'Processing Order:%d Product:%d Supplier:%s', $order->{id}, $product_id, $product->{supplier} );

            my $items_avail  = $self->storage->items_avail( $supplier, $product->{merlion_vid} );
            my $selected_sms = $self->config->{supplier}{merlion}{use_shipment_methods};
            my ( $prefered_avail )
                = sort {
                    $selected_sms->{ $a->{shipment_code} } <=> $selected_sms->{ $b->{shipment_code} }
                }
                grep { $_->{cnt} > 0 }
                @$items_avail;

            if ( !$prefered_avail ) {
                $log->trace( 'Skipping. No items avail.' );
                next PRODUCTS;
            }

            $self->storage->add_order( {
                order_id           => $order->{id},
                product_id         => $product_id,
                foreign_product_id => $prefered_avail->{item_id},
                supplier           => $supplier,
                shipment_code      => $prefered_avail->{shipment_code},
                count              => $cart->{ $product_id }{num},
            } );
            last NEW_ORDERS;
        }
    }
}

sub orders_send {
    my ( $self ) = @_;

    my $lines = $self->storage->get_orders_lines;
    my $shipment_info = $self->shipment_info( 'merlion' );

    my %shipment_methods_map = map { $_->{code} => $_; } @$shipment_info;

    ORDER_LINES:
    for my $line ( @$lines ) {
        my $shipment_method = $shipment_methods_map{ $line->{shipment_code} };

        my $order = $self->supplier('merlion')->get_order(
            shipment_code => $shipment_method->{code},
            shipment_date => $shipment_method->{shipment_date},
        );

        if ( !%$order ) {
            $order = $self->supplier('merlion')->create_order(
                shipment_code => $shipment_method->{code},
                shipment_date => $shipment_method->{shipment_date},
            );
        }

        my $command_res = $self->supplier('merlion')->push_order_line( $order, $line );
        $self->storage->set_order_line( $line, $command_res );
    }
}

sub shipment_info {
    my ( $self, $supplier ) = @_;

    my %shipment_info_of = (
        merlion => sub {
            my $sms = $self->storage->get_shipment_info( $supplier );
            if ( !@$sms ) {
                my $new_sms = $self->supplier( $supplier )->shipment_methods;
                $self->storage->set_shipment_info( $supplier, $new_sms );
                $sms = $self->storage->get_shipment_info( $supplier );
            }
            return $sms;
        },
        ocs => sub {
            return [ keys %{ $self->config->{supplier}{ocs}{shipment} } ];
        },
        marvel => sub {
            return [ keys %{ $self->config->{supplier}{marvel}{shipment} } ];
        },
        elko => sub {
            return [ keys %{ $self->config->{supplier}{marvel}{shipment} } ];
        },
        treolan => sub { [] },
        tonl    => sub { [] },
        nlab    => sub { [ keys %{ $self->config->{supplier}{nlab}{shipment} } ] },
        '3log'  => sub { [] },
    );
    if (exists $shipment_info_of{ $supplier }) {
        return $shipment_info_of{ $supplier }->();
    }
    else {
        return [];
    }
}

1;
