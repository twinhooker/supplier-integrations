package SI::ShopClient;
use strict;
use warnings;

use DBI;
use Log::Any '$log';
use PHP::Serialization qw(serialize unserialize);
use Readonly;
use SQL::Abstract::More;
use Time::Piece;
use SQL::Abstract::Plugin::InsertMulti;

Readonly my $ORDER_STATUS_NEW        => 0;
Readonly my $ORDER_STATUS_PROCESSING => 106;
Readonly my %API_SUPPLIERS => (
    '3log'    => 1,
    'elko'    => 1,
    'marvel'  => 1,
    'merlion' => 1,
    'nlab'    => 1,
    'ocs'     => 1,
    'treolan' => 1,
    'tonl'    => 1,
);

use Class::Tiny qw( dsn username password ), {
    dbh => sub { $_[0]->_build_dbh },
    sql => sub { SQL::Abstract::More->new },
};

sub _build_dbh {
    my $self = shift;

    my $dbh = DBI->connect( $self->dsn, $self->username, $self->password, {
        mysql_enable_utf8 => 1,
        RaiseError        => 1,
        AutoCommit        => 1,
    } );

    return $dbh;
}

sub new_orders {
    my $self = shift;

    my %where = (
        statusi => { -in  => [ $ORDER_STATUS_NEW, $ORDER_STATUS_PROCESSING ] },
        datas   => { '>=' => \"UNIX_TIMESTAMP( CURDATE() )" }
    );

    my ( $stmt, @bind ) = $self->sql->select(
        -from    => 'phpshop_orders',
        -columns => '*',
        -where   => \%where
    );
    my $orders = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );
    map { $_->{orders} = unserialize( $_->{orders} ) } @$orders;

    return $orders;
}

sub get_category {
    my ( $self, %args ) = @_;

    my ( $stmt, @bind ) = $self->sql->select( 'phpshop_categories', '*', \%args );
    my $category = $self->dbh->selectrow_hashref( $stmt, { Slice => {} }, @bind );
    if ( $category ) {
        $category->{sort} = unserialize( $category->{sort} );
    }
    return $category;
}

sub set_category {
    my ( $self, $category ) = @_;

    my %data = ( sort => serialize( $category->{sort} ) );
    $log->infof( 'Updating Category %d: %s', $category->{id}, \%data );

    my ( $stmt, @bind ) = $self->sql->update( 'phpshop_categories', \%data, { id => $category->{id} } );
    $self->dbh->do( $stmt, undef, @bind );

    return;
}

sub enable_product {
    my ( $self, $product, $p_enabled ) = @_;

    $p_enabled ||= '0';
    $log->infof( "Updating product(%d) p_enabled to %s sklad. Have new positions.", $product->{id}, $p_enabled );
    my %enable_options = (
        sklad                 => '0',
        enabled               => '1',
        yml                   => '1',
        ekatalog              => '1',
        p_enabled             => $p_enabled,
        status_changed_by_api => 1,
    );
    my ( $stmt, @bind ) = $self->sql->update( 'phpshop_products', \%enable_options, { id => $product->{id} } );
    $self->dbh->do( $stmt, undef, @bind );

    if ( $product->{parent_enabled} && $product->{parent} ) {
        my @ids = split( ',', $product->{parent} );
        $log->infof( 'Product(%d) has parent(%s). Enabling it too.', $product->{id}, \@ids ); # $product->{parent} );
        my ( $stmt1, @bind1 ) = $self->sql->update( 'phpshop_products', \%enable_options, { id => { -in => \@ids } } ); # $product->{parent} } );
        $self->dbh->do( $stmt1, undef, @bind1 );
    }

    return;
}

sub disable_product {
    my ( $self, $product ) = @_;

    $log->infof( 'Updating product(%d) sklad. Have no positions.', $product->{id} );
    my %disable_options = (
        sklad                 => '1',
        yml                   => '0',
        ekatalog              => '0',
    );
    my ( $stmt, @bind ) = $self->sql->update( 'phpshop_products', \%disable_options, { id => $product->{id} } );
    $self->dbh->do( $stmt, undef, @bind );

    return;
}

#
# Shop Section
#

sub get_suppliers {
    my ( $self, %where ) = @_;

    my $check_has_api = delete $where{has_api};
    my ( $stmt, @bind ) = $self->sql->select(
        -columns => '*',
        -from    => 'phpshop_supplier',
        -where   => \%where
    );
    my $suppliers = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

    my @suppliers;
    SUPPLIERS:
    for my $supplier ( @$suppliers ) {
        if (exists $API_SUPPLIERS{ $supplier->{supplier_field} }) {
            $supplier->{has_api} = 1;
        }

        if ($check_has_api) {
            push @suppliers, $supplier if ($supplier->{has_api});
        }
        else {
            push @suppliers, $supplier;
        }
    }

    return \@suppliers;
}

sub get_products {
    my ( $self, %args ) = @_;

    my %where = %args;
    my ( $stmt, @bind ) = $self->sql->select(
        -columns => '*',
        -from    => 'phpshop_products',
        -where   => \%where
    );
    my $items = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

    return $items;
}

sub get_category_ids {
    my ( $self, $parent_cat_id ) = @_;


    my $cat_query = q{select id from
                          ( select * from phpshop_categories order by parent_to, id ) products_sorted,
                          ( select @pv := ? ) initialisation
                      where find_in_set( parent_to, @pv )
                      and   length( @pv := concat( @pv, ',', id ) )};
    my $cat_ids = $self->dbh->selectcol_arrayref( $cat_query, undef, $parent_cat_id );

    return $cat_ids;
}

sub get_updatable_products {
    my ( $self, %args ) = @_;

    my @update_required;
    my ( $stmt, @bind ) = $self->sql->select(
        -columns => [
            'pp.*',
            'count(psp.id)|spec_count',
            'count(pspc.id)|custom_spec_count',
            'count(pspi.product_id)|import_spec_count'
        ],
        -from => [
            -join => 'phpshop_products|pp',
            {
                operator  => '=>',
                condition => { 'pp.id' => { '=' => { -ident => 'psp.id' } } },
            },
            'phpshop_specification_products|psp',
            {
                operator => '=>',
                condition => { 'pp.id' => { '=' => { -ident => 'pspc.id' } } },
            },
            'phpshop_specification_products_custom|pspc',
            {
                operator => '=>',
                condition => { 'pp.id' => { '=' => { -ident => 'pspi.product_id' } } },
            },
            'phpshop_specification_import_list|pspi',
        ],
        -where => {
            # 'pp.supplier'    => 'merlion',
            'pp.merlion_vid' => { '!=' => [ -and => ( '', undef ) ] },
        },
        -group_by => [ 'pp.id' ],
    );
    my $items = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

    return $items;
}

sub set_product {
    my ( $self, $product ) = @_;

    my %data = (
        ( $product->{vendor_array} )      ? ( vendor_array => serialize( $product->{vendor_array} ) ) : (),
        ( $product->{pic_big} )           ? ( pic_big      => $product->{pic_big} ) : (),
        ( $product->{pic_small} )         ? ( pic_small    => $product->{pic_small} ) : (),
        ( $product->{supplier} )          ? ( supplier     => $product->{supplier} ) : (),
        ( $product->{price} )             ? ( price        => $product->{price} ) : (),
        ( $product->{start_price} )       ? ( start_price  => $product->{start_price} ) : (),
        ( exists $product->{ignore_api} ) ? ( ignore_api   => $product->{ignore_api} ) : (),
        ( $product->{has_merlion_description} ) ? ( has_merlion_description => $product->{has_merlion_description} ) : (),
    );
    # $data{has_merlion_description} = 1
    #     if ( $product->{pic_big} || $product->{pic_small} || $product->{vendor_array} );

    $log->infof( 'Updating product %d data: %s', $product->{id}, \%data );
    my ( $stmt, @bind ) = $self->sql->update( 'phpshop_products', \%data, { id => $product->{id} } );
    $self->dbh->do( $stmt, undef, @bind );

    return;
}

#
# Specs
#

sub add_product_specs {
    my ( $self, $product_id, $supplier, $item_properties ) = @_;

    FOREIGN_PROPERTIES:
    for my $item_property ( @$item_properties ) {
        next FOREIGN_PROPERTIES if ( length $item_property->{Value} > 64 );
        next FOREIGN_PROPERTIES if ( length $item_property->{PropertyName} > 64 );

        my $stmt = "INSERT IGNORE INTO phpshop_specification_import_list (product_id, supplier, spec_name, value_name) VALUES (?, ?, ?, ?)";
        my @bind = ( $product_id, $supplier, $item_property->{PropertyName}, $item_property->{Value} );
        $log->tracef("Product(%s:%s) %s = %s", @bind );
        $self->dbh->do( $stmt, undef, @bind );
    }

    return;
}


#
# Properties
#
sub get_or_create_property {
    my ( $self, %args ) = @_;

    $log->tracef( 'Get or create section "%s": %s', $args{name}, \%args );
    my $properties = $self->find_property( %args );
    if ( !@$properties ) {
        $self->create_property( %args );
        $properties = $self->find_property( %args );
    }

    return $properties->[0];
}

sub find_property {
    my ( $self, %args ) = @_;

    $log->tracef( 'Find property "%s": %s', $args{name} || '', \%args );
    my ( $stmt, @bind ) = $self->sql->select( 'phpshop_sort_categories', '*', \%args );
    my $sections = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

    return $sections;
}

sub create_property {
    my ( $self, %args ) = @_;

    $log->tracef( 'Create property section: %s', \%args );
    my ( $stmt, @bind ) = $self->sql->insert( 'phpshop_sort_categories', \%args );

    $self->dbh->do( $stmt, undef, @bind );
}

#
# Property values
#

sub get_or_create_property_value {
    my ( $self, %args ) = @_;

    $log->tracef( 'Get or create property value "%s" : %s', $args{name}, \%args );
    my $values = $self->find_property_value( %args );

    if ( !@$values ) {
        $self->create_property_value( %args );
        $values = $self->find_property_value( %args );
    }

    return $values->[0];
}

sub find_property_value {
    my ( $self, %args ) = @_;

    $log->tracef( 'Find property value "%s": %s', $args{name} || '', \%args );
    my ( $stmt, @bind ) = $self->sql->select( 'phpshop_sort', '*', \%args );
    my $values = $self->dbh->selectall_arrayref( $stmt, { Slice => {} }, @bind );

    return $values;
}

sub create_property_value {
    my ( $self, %args ) =  @_;

    $log->tracef( 'Create property value"%s": %s', $args{name}, \%args );
    $args{pic_url} ||= '';
    my ( $stmt, @bind ) = $self->sql->insert( 'phpshop_sort', \%args );
    $self->dbh->do( $stmt, undef, @bind );
}

1;
