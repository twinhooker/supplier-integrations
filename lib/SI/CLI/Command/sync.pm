package SI::CLI::Command::sync;
use strict;
use warnings;
use utf8;
use Log::Any '$log';
use SI::CLI -command;
use SI::Synchronizer;
use SI::DescriptionLoader;
use Time::Piece;

sub execute {
    my ( $self, $opt, $args ) = @_;

    my $section = shift @$args;
    my %sync_sections = (
        avail    => sub { $self->sync_avail( $opt ) },
        orders   => sub { $self->sync_orders( 'merlion' ) },
        products => sub { $self->sync_products( $opt, $args ) },
    );
    $sync_sections{ $section }->();
}

sub options { return (
    [ 'force'      => 'Force' ],
    [ 'cat_id=i'   => 'Category ID' ],
    [ 'supplier=s' => 'Supplier' ]
); }

sub validate { }

sub sync_avail {
    my ( $self, $opt ) = @_;

    my $synchronizer = SI::Synchronizer->new(
        config_path => $self->{config_path},
        log_path    => $self->{log_path},
       (log_level   => $opt->{log_level}) x!! $opt->{log_level}
    );

    my %supplier_opts = ( has_api => 1, api_enable => 1 );
    %supplier_opts    = ( supplier_field => $opt->{supplier} )
        if ( $opt->{supplier} );

    my $suppliers = $synchronizer->shop->get_suppliers( %supplier_opts );
    my @suppliers = map { $_->{supplier_field} } @$suppliers;
    $log->infof("Processing following suppliers %s.", \@suppliers);

    for my $supplier ( @suppliers ) {
        eval { $synchronizer->sync_avail( $supplier ); };
        if ( $@ ) { $log->errorf( 'ERROR: %s', $@ ); }
    }

    if ( !$opt->{supplier} ) {
        $synchronizer->update_availability;
        $synchronizer->check_sklad_products;
    }

    return;
}

sub sync_products {
    my ( $self, $opt, $args ) = @_;

    my $desc_loader = SI::DescriptionLoader->new(
        config_path => $self->{config_path},
        log_path    => $self->{log_path}
    );

    if ( $opt->{force} ) {
        $desc_loader->force_load({
            ( cat_id => $opt->{cat_id} ) x!! $opt->{cat_id},
        });
    }
    else {
        $desc_loader->load_descriptions;
    }

    return;
}

sub sync_orders {
    my ( $self, $supplier ) = @_;
    # do nothing between 17:00 and 18:00
    if ( localtime->hour == 17 ) {
        $log->info( 'Doing nothing between 17:00 and 18:00' );
        return;
    }
    my $synchronizer = SI::Synchronizer->new(
        config_path => $self->{config_path},
        log_path    => $self->{log_path}
    );
    $synchronizer->sync_orders( 'merlion' );

    return;
}

1;
