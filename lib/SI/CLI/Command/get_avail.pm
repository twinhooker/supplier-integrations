package SI::CLI::Command::get_avail;
use common::sense;
use Log::Any '$log';
use SI::CLI -command;
use SI::Synchronizer;
use Data::Dumper::Perltidy;
sub options {
    return (
        [ 'id=i',          'Shop product id' ],
        [ 'supplier=s',    'Supplier name' ],
        [ 'supplier_id=s', 'Supplier product id' ],
        [ 'api_response',  'ASK API for data' ]
    );
}

sub validate {
    my ( $self, $opt, $args ) = @_;

    die 'product id or supplier name and supplier product id required.'
        if ( !$opt->{id} && !( $opt->{supplier} && $opt->{supplier_id} ) );
}

sub execute {
    my ( $self, $opt, $args ) = @_;

    my $products = $self->api->shop->get_products( id => $opt->{id} );
    my $product  = shift @$products;

    my ( $cat_sql, @cat_bind )
        = $self->api->shop->sql->select( 'phpshop_categories', '*', { id => $product->{category} } );
    my $cats = $self->api->shop->dbh->selectall_arrayref( $cat_sql, { Slice => {} }, @cat_bind );
    my %cat_info     = %{ $cats->[0] }{ qw( ignore_api ) };
    my %product_info = %{ $product }{ qw( ignore_api sklad p_enabled supplier parent_enabled parent ) };
    $log->infof( "%s", $product );
    $log->info( Dumper( \%product_info ) );
    $log->info( Dumper( \%cat_info ) );

    my $suppliers = $self->api->shop->get_suppliers(has_api => 1, api_enable => 1);
    my @suppliers = map { $_->{supplier_field} } @$suppliers;

    my $avail_info = $self->api->storage->avail_info(
        api_suppliers => \@suppliers,
        'p.id'         => { -in => $product->{id} },
        'c.ignore_api' => { -in => [ 0, 1, 2 ] },
        dont_exclude_temp_ignore_api => 1,
    );
    my $avail_info = shift @$avail_info;
    $log->info( Dumper( $avail_info ) );
    my $api_enabled_group = ( $avail_info->{ignore_api} == 0 ) ? 'ON/OFF'
                          : ( $avail_info->{ignore_api} == 1 ) ? 'ONLY ON'
                          : ( $avail_info->{ignore_api} == 2 ) ? 'IGNORE'
                          : ( $avail_info->{ignore_api} == 3 ) ? 'CHECK CATEGORY'
                          :                                      'ERROR';
    my $api_enabled_product = ( $avail_info->{p_ignore_api} == 0 ) ? 'ON/OFF'
                            : ( $avail_info->{p_ignore_api} == 1 ) ? 'ONLY ON'
                            : ( $avail_info->{p_ignore_api} == 2 ) ? 'IGNORE'
                            : ( $avail_info->{p_ignore_api} == 3 ) ? 'CHECK CATEGORY'
                            :                                        'ERROR';
    my $is_enabled = ( $avail_info->{sklad} eq '0' ) ? 'Enabled' : 'Disabled';

    $log->info( sprintf( 'API enable for group: %s', $api_enabled_group ) );
    $log->info( sprintf( 'API enable for product: %s', $api_enabled_product ) );
    $log->info( sprintf( 'Sklad Product - %s', $is_enabled ) );
    $log->info( sprintf( 'Product Enable - %s', $avail_info->{enabled} ) );

    if ( $opt->{supplier} ) {
        @suppliers = ( $opt->{supplier} );
        my $supplier_id_key = sprintf( '%s_vid', $opt->{supplier} );
        $log->infof( 'Supplier VendorID: %s', $product->{$supplier_id_key} );
    }
    use SI::Synchronizer;
    my $synchronizer = SI::Synchronizer->new(
        config_path => $self->{config_path},
        log_path    => $self->{log_path},
       (log_level   => $self->{log_level}) x!! $self->{log_level}
    );
    my $best_supplier = $synchronizer->_best_supplier( \@suppliers, $avail_info );
    $log->infof('Best supplier: %s', $best_supplier );

    if ( $opt->{api_response} ) {
        my $supplier_id_key = sprintf( '%s_vid', $opt->{supplier} );
        my $shipment_info = $self->api->shipment_info( $opt->{supplier} );
        my $supplier_info = $self->api->supplier( $opt->{supplier} )->items_avail(
            product_ids   => [ $product->{ $supplier_id_key } ],
            shipment_info => $shipment_info,
        );
        $log->info( $supplier_id_key . '=' . $product->{ $supplier_id_key } );
        $log->info( Dumper( $supplier_info ) );
    }

    return;
}

1;
