package SI::CLI::Command::orders;
use strict;
use warnings;

use Time::Piece;
use SI::CLI -command;
use SI::Synchronizer;
use Readonly;
use Log::Any '$log';

sub execute {
    my ( $self, $opt, $args ) = @_;

    my $command = shift @$args;
    print "TEST\n";
    if ( $command eq 'send' ) {
        print "TEST2\n";
        $self->orders_send($opt);
    }
}

sub options { return (); }
sub validate { }

sub orders_send {
    my ( $self, $opt ) = @_;

    # do nothing between 17:00 and 18:00
    if ( localtime->hour == 17 ) {
        $log->info( 'Doing nothing between 17:00 and 18:00' );
        return;
    }

    my $synchronizer = SI::Synchronizer->new(
        config_path => $self->{config_path},
        log_path    => $self->{log_path},
       (log_level   => $opt->{log_level}) x!! $opt->{log_level}
    );
    $synchronizer->orders_send;

    return;
}

1;
