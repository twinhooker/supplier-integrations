package SI::CLI::Command;

use strict;
use warnings;

use App::Cmd::Setup -command;
use SI::API;
use Log::Log4perl qw( :easy );
use Log::Any::Adapter;
use String::Flogger;
use Log::Any '$log';

use Class::Tiny qw(), {
    api => sub { $_[0]->_build_api },
};

sub opt_spec {
    my ( $class, $app ) = @_;

    return (
        [ 'config|c=s'  => 'Config_file' ],
        [ 'log|l=s'     => 'Log path.' ],
        [ 'log_level=s' => 'Log level.'],
        $class->options()
    );
}

sub validate_args {
    my ( $self, $opt, $args ) = @_;

    die $self->usage_error( 'No config file.' ) if ( !$opt->{config} );
    $self->{config_path} = $opt->{config};
    $self->{log_path}    = $opt->{log};
    $self->{log_level}   = $opt->{log_level} if ( $opt->{log_level} );
    $self->validate( $opt, $args );
}

sub _build_api {
    my $self = shift;

    my $api = SI::API->new(
        config_path => $self->{config_path},
        log_path    => $self->{log_path},
       (log_level   => $self->{log_level}) x!! $self->{log_level}
    );

    return $api;
}

1;

