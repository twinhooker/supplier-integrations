package SI::DescriptionLoader;
use utf8;
use common::sense;
use parent qw( SI::API );
use Log::Any '$log';

sub force_load {
    my ( $self, $args ) = @_;

    my $cat_ids = $self->shop->get_category_ids( $args->{cat_id} );
    my $products = $self->shop->get_products(
        supplier    => 'merlion',
        merlion_vid => { '!=' => [ -and => ( '', undef ) ] },
        category    => [ -in => $cat_ids ]
    );
    my %product_having = map { ( $_->{merlion_vid} => $_ ) } @$products;
    my $sections       = $self->supplier('merlion')->get_item_property_sections;

    $log->infof( 'Going to process %d products', scalar keys %product_having );

    MERLION_PRODUCTS:
    for my $merlion_id ( keys %product_having ) {
        my $product = $product_having{ $merlion_id };
        $log->infof( 'Processing product( ID:%d, merlion_vid:%d ).', @{ $product }{qw( id merlion_vid )} );
        $product->{vendor_array} = $self->load_description_for( $merlion_id, $sections );

        $log->infof( 'Product pictures %s', { %{ $product }{qw( pic_big pic_small )} } );
        my $image_map = $self->supplier('merlion')->get_item_images( $product->{merlion_vid} );

        PICTURES:
        for (qw( pic_big pic_small )) {
            if ( $image_map->{ $_ } ) {
                my $image_rel_path = $self->supplier('merlion')->download( $image_map->{ $_ } );
                $product->{ $_ } = $image_rel_path;
                $log->infof( 'Downloaded product %d picture %s', $product->{id}, $image_rel_path )
            }
        }

        # Check if category has all properties
        my $category            = $self->shop->get_category( id => $product->{category} );
        my %property_categories = map {( $_ => undef )} @{ $category->{sort} };

        PROPERTY_CAT_IDS:
        for my $property_cat_id ( keys %{ $product->{vendor_array} } ) {
            push @{ $category->{sort} }, $property_cat_id
                if ( ! exists $property_categories{ $property_cat_id } );
        }

        $self->shop->set_category( $category );
        $self->shop->set_product( $product );
    }

    return;
}

sub load_descriptions {
    my ( $self ) = @_;

    my $empty_products = $self->shop->get_updatable_products();
    my %product_having = map { ( $_->{merlion_vid} => $_ ) } @$empty_products;

    my $i = 0;
    my $sections = $self->supplier('merlion')->get_item_property_sections;

    $log->infof( 'Going to process %d products', scalar keys %product_having );
    MERLION_PRODUCTS:
    for my $merlion_id ( keys %product_having ) {
        my $product = $product_having{ $merlion_id };

        $log->tracef('Processing product %d', $product->{id});
        if (    $product->{spec_count}        == 0
             && $product->{custom_spec_count} == 0
             && $product->{import_spec_count} == 0
        ) {
            eval {
                $log->infof('Loading description for Product(%s).', $product->{id} );
                my $item_properties = $self->load_description_for( $merlion_id, $sections );
                $self->shop->add_product_specs( $product->{id}, $product->{supplier}, $item_properties );
            };
            if ($@) { $log->errorf( 'Load description error: %s', $@ ); }
        }

        if ( !$product->{pic_big} || !$product->{pic_small} ) {
            $log->infof( 'Pictures %s', { %{ $product }{qw( pic_big pic_small )} } );

            my $image_map = $self->supplier('merlion')->get_item_images( $product->{merlion_vid} );
            $log->tracef( 'Got pictures' );
            for (qw( pic_big pic_small )) {
                if ( !$product->{ $_ } && $image_map->{ $_ } ) {
                    my $image_rel_path = $self->supplier('merlion')->download( $image_map->{ $_ } );

                    $product->{ $_ } = $image_rel_path;
                    $log->infof( 'Downloaded product %d picture %s', $product->{id}, $image_rel_path )
                }
            }
            $self->shop->set_product( $product, 1 );
        }

        # Check if category has all properties
        # my $category            = $self->shop->get_category( id => $product->{category} );
        # my %property_categories = map {( $_ => undef )} @{ $category->{sort} };
        # PROPERTY_CAT_IDS:
        # for my $property_cat_id ( keys %{ $product->{vendor_array} } ) {
        #     push @{ $category->{sort} }, $property_cat_id
        #         if ( ! exists $property_categories{ $property_cat_id } );
        # }
        # $self->shop->set_category( $category );
        # 

        # last MERLION_PRODUCTS;
    }
}

sub load_description_for {
    my ( $self, $merlion_id, $sections ) = @_;

    my $item_properties = $self->supplier('merlion')->get_items_properties( item_id => [ $merlion_id ] );

    return $item_properties;
    # FOREIGN_PROPERTIES:
    # for my $foreign_property ( @$item_properties ) {
    #     $log->infof("%s - %s", $foreign_property->{PropertyName}, $foreign_property->{Value});        
    # }
    #     next FOREIGN_PROPERTIES if ( length $foreign_property->{Value} > 64 );
    #     next FOREIGN_PROPERTIES if ( length $foreign_property->{PropertyName} > 64 );

    #     my $property_section_name = ( $foreign_property->{Section_Id} )
    #         ? $sections->{ $foreign_property->{Section_Id} }
    #         : 'Основные характеристики';

    #     my $property_section = $self->shop->get_or_create_property(
    #         name     => $property_section_name,
    #         category => 0
    #     );

    #     my $property = $self->shop->get_or_create_property(
    #         name     => $foreign_property->{PropertyName},
    #         category => $property_section->{id},
    #     );

    #     my $property_value = $self->shop->get_or_create_property_value(
    #         category => $property->{id},
    #         name     => $foreign_property->{Value}
    #     );

    #     $vendor_array{ $property->{id} } = [ $property_value->{id} ];
    # }
    # $log->infof( 'Downloaded product ( merlion_vid: %d ) vendor_array %s.', $merlion_id, \%vendor_array );

    # return \%vendor_array;
}

1;
