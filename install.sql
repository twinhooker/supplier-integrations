create table si_orders (
    order_id           int,
    product_id         int,
    foreign_product_id int,
    supplier           varchar(255),
    shipment_code      varchar(255),
    count              int,
    status             int default 0,
    unique key `si_orders_keys` ( `order_id`, `product_id`, `supplier` )
);

create table si_merlion_shipmentmethods (
    `code` varchar(255),
    `shipment_date` date,
    `date` timestamp
);

create table si_merlion_avail (
    merlion_id int,
    shipment_code varchar(255),
    cnt int,
    unique key `si_merlion_avail_key` ( `merlion_id`, `shipment_code` )
);

create table si_ocs_avail (
    item_id int,
    shipment_city varchar(255),
    cnt int,
    unique key `si_ocs_avail_key` ( `item_id`, `shipment_city` )
);

create table si_marvel_avail (
    item_id varchar(255),
    shipment_city varchar(255),
    cnt int,
    primary key ( `item_id` )
) DEFAULT CHARSET=utf8;

create table si_elko_avail (
    item_id varchar(255),
    shipment_city varchar(255),
    cnt int,
    unique key `si_ocs_avail_key` ( `item_id`, `shipment_city` )
) DEFAULT CHARSET=utf8;

create table si_csv_avail (
    supplier varchar(255),
    item_id  varchar(255),
    cnt      int,
    price    decimal( 10, 2 ),
    unique key si_csv_avail_key ( `supplier`, `item_id` )
) DEFAULT CHARSET=utf8;

create table si_tonl_avail (
    item_id       varchar(255),
    shipment_city varchar(255),
    cnt           int,
    price         decimal( 10, 2 ),
    unique key `si_ocs_avail_key` ( `item_id`, `shipment_city` )
) DEFAULT CHARSET=utf8;

alter table si_csv_avail add unique index ( `supplier`, `item_id` );
alter table si_ocs_avail add column price float default null;
alter table si_elko_avail add column price float default null;
alter table si_marvel_avail add column price float default null;
alter table si_merlion_avail change column merlion_id item_id int;
alter table si_merlion_avail add column price float default null;

create table si_treolan_avail (
    item_id varchar(255),
    shipment_city varchar(255),
    cnt int,
    price float default null,
    primary key ( `item_id` )
) DEFAULT CHARSET=utf8;

create table si_nlab_avail (
    item_id       varchar(255),
    shipment_city varchar(255),
    cnt           int,
    price         decimal( 10, 2 ),
    unique key `si_nlab_avail_key` ( `item_id`, `shipment_city` )
) DEFAULT CHARSET=utf8;

alter table phpshop_supplier add column delivery_price float default null;
alter table phpshop_supplier add column delivery_currency enum('RUB', 'USD') not null default 'RUB';
alter table phpshop_supplier add column delivery_second_price_border float default null;
alter table phpshop_supplier add column delivery_second_price float default 0;
alter table phpshop_supplier add column api_enable boolean default false;
update phpshop_supplier set delivery_price = 2, delivery_currency = 'USD', delivery_second_price_border = 1000 where supplier_field = 'nlab';
update phpshop_supplier set delivery_price = 2000, delivery_currency = 'RUB', delivery_second_price_border = 100000, delivery_second_price = 3000 where supplier_field = 'tonl';
update phpshop_supplier set delivery_price = 1700, delivery_currency = 'RUB', delivery_second_price_border = 60000,  delivery_second_price = 0    where supplier_field = 'treolan';
update phpshop_supplier set api_enable = 1 where supplier_field in ('ocs', 'treolan', 'tonl', 'nlab', 'merlion', 'marvel', 'elko' );


alter table si_nlab_avail add column absent int default 0;

alter table phpshop_products add column temp_ignore_api timestamp default 0;
