#!/usr/bin/perl
use Test2::V0;
use App::Cmd::Tester;
use SI::CLI;
use Data::Dumper;

my $result = eval {
    test_app( 'SI::CLI' => [qw( orders send --config ./config.conf --log ./test.log )] );
};

warn Dumper( $@ )    if ( $@ );
warn $result->stderr if $result->stderr;
is $result->error, undef;

done_testing;
