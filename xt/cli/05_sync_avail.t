#!/usr/bin/perl
use Test2::V0;
use App::Cmd::Tester;
use SI::CLI;
use Data::Dumper;

my @args = qw( sync avail );
my $result = eval {
    test_app( 'SI::CLI' => [ @args, qw( --config ./config.conf --log ./test.log )] );
};
if ( $@ ) {
    warn Dumper( $@ );
};
warn $result->stderr if $result->stderr;
is $result->error, undef;

done_testing;

