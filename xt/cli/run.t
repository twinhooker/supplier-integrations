#!/usr/bin/perl
use Test2::V0;
use App::Cmd::Tester;
use SI::CLI;
use Data::Dumper;

my @commands = (
    [qw( sync avail )],
    # [qw( sync orders )],
    # [qw( sync products )],
    # [qw( orders send )],
    # [qw( get_avail --id 101352 )]
);

for my $command ( @commands ) {
    my $result = eval { test_app( 'SI::CLI' => [ @$command, qw( --config ./config.conf --log ./test.log )] ); };
    if ( $@ ) {
        warn Dumper( $@ );
    };
    warn $result->stderr if $result->stderr;
    is $result->error, undef;
}

done_testing;
